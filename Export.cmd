@echo off
echo - Exporting Skype data
powershell.exe -ExecutionPolicy Bypass -NoLogo -NoProfile -File "%~dp0Use-SkypeProtectedData.ps1" -Export -Verbose
echo - Done
pause