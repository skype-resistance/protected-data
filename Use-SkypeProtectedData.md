# Use-SkypeProtectedData.ps1

## SYNOPSIS
Import/Export Skype "salt" used to decrypt Credentials3 in config.xml

## SYNTAX

### Export (Default)
```
Use-SkypeProtectedData.ps1 [-Export] [<CommonParameters>]
```

### Import
```
Use-SkypeProtectedData.ps1 [-Import] [-Path] <String> [<CommonParameters>]
```

## DESCRIPTION
Import/Export Skype "salt" used to decrypt Credentials3 in config.xml
This allows you to move config.xml files between different machines.

Salt is stored in the "HKEY_CURRENT_USER\Software\Skype*\ProtectedStorage"
registry keys and protected with DPAPI.

Skype 7 uses "Skype" key, while newer versions use "skypeapp-xxxxxxxxxxxx" keys.

Export will get data from all Skype instances, which then can be imported
into the Skype 7 registry key

## EXAMPLES

### EXAMPLE 1
```
& .\Use-SkypeProtectedData.ps1 -Export
```

Export Skype salt.

### EXAMPLE 2
```
& .\Use-SkypeProtectedData.ps1 -Import -Path .\MYPC_Skype_2019-04-10T04.05.14.9391042+03.00.xml
```

Import Skype salt.

## PARAMETERS

### -Export
Switch.
Script will export registry data to XML file.

```yaml
Type: SwitchParameter
Parameter Sets: Export
Aliases:

Required: True
Position: 1
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Import
Switch.
Script will export registry from to XML file.

```yaml
Type: SwitchParameter
Parameter Sets: Import
Aliases:

Required: True
Position: 1
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Path
Path to XML file.
Can be omited for Export.

```yaml
Type: String
Parameter Sets: Import
Aliases:

Required: True
Position: 2
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable.
For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
