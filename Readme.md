# Import/Export Skype "salt" used to decrypt Credentials3 in config.xml

## Details

1. Run `Export.cmd` on the PC with working Skype.
2. Copy script folder with resulting XML backup files to the new PC.
3. Copy `config.xml` containing `Credentials3` autologin data from this PC to the Skype profile on the new PC.
4. Run `Import.cmd` and specifiy XML file name.

Skype 7 shoud be able to use credentials from `config.xml` to autologin.

See [Use-SkypeProtectedData.md](Use-SkypeProtectedData.md) for detailed help.

## Usage

### Export

```shell
Export.cmd
```

### Import

```shell
Import.cmd MYPC_Skype_2019-04-10T04.05.14.9391042+03.00.xml
```

or run `Export.cmd` w/o parameters and enter XML filename when asked.

```shell
Import.cmd

- Importing Skype data
- Enter XML file name: MYPC_Skype_2019-04-10T04.05.14.9391042+03.00.xml
- File: MYPC_Skype_2019-04-10T04.05.14.9391042+03.00.xml
- Done
Press any key to continue . . .
```
