@echo off
echo - Importing Skype data
if [%1]==[] (
  set /P file=- Enter XML file name: 
) else (
  set file=%1
)
echo - File: %file%
powershell.exe -ExecutionPolicy Bypass -NoLogo -NoProfile -File "%~dp0Use-SkypeProtectedData.ps1" -Import -Path "%file%" -Verbose
echo - Done
pause